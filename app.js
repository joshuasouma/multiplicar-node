const { argv } = require('./config/yargs');
const colors = require('colors');

//const colors = require('colors/safe');
//colors.green(String);

const { crearArchivo, listarTabla } = require('./multiplicar/multiplicar.js');


let comando = argv._[0];
let base = argv.base;
let limite = argv.limite;




switch (comando) {
    case 'listar':
        listarTabla(base, limite)
            .then(lista => console.log(lista.yellow))
            .catch(err => console.log(err));

        break;
    case 'crear':
        crearArchivo(base, limite)
            .then(archivo => console.log(`Archivo creado: ${archivo.rainbow}`))
            .catch(err => console.log(err));

        break;

    default:
        console.log('Comando no reconocido');

        break;
}

//console.log(argv.base);



// let parametro = argv[2];
// let base = parametro.split("=")[1];
//console.log(process.argv);