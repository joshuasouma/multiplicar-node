const fs = require('fs');

let listarTabla = async(base, limite) => {

    if (!Number(base)) {
        throw new Error(`El valor introducido '${base}' no es un número.`);
    } else if (!Number(limite)) {
        throw new Error(`El valor introducido '${limite}' no es un número.`);

    }

    let data = '';

    for (let i = 1; i <= limite; i++) {

        data += `${base} * ${i} = ${base*i}\n`;
    }
    return data;
}

let crearArchivo = async(base, limite) => {

    if (!Number(base)) {
        throw new Error(`El valor introducido '${base}' no es un número.`);
    } else if (!Number(limite)) {
        throw new Error(`El valor introducido '${limite}' no es un número.`);

    }

    let data = '';

    for (let i = 1; i <= limite; i++) {

        data += `${base} * ${i} = ${base*i}\n`;
    }

    fs.writeFile(`tablas/tabla-${base}-al-${limite}.txt`, data, (err) => {
        if (err) throw new Error(err);

    });
    return `tabla-${base}.txt`;

}

module.exports = {
    crearArchivo,
    listarTabla

};